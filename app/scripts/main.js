/* 
  FUNTION PARA FIXAR BARRA AO TOPO 
*/
const nav = document.querySelector('#nav-bar');
const preNav = document.querySelector('#preNav');
const navTop = nav.offsetTop;

//Está função funciona como um toggle para a classe sticky
//set é um parametro onde quando for verdadeiro ira adicionar as classes sem esperar o scroll
function stickyNavigation(set) {

  if (window.scrollY > navTop || set == true) {
    nav.classList.add('sticky');
    nav.classList.add('animation-nav-bar');
    preNav.classList.add('sticky-pre-nav');
  } else {
    nav.classList.remove('sticky');
    nav.classList.remove('animation-nav-bar');
    preNav.classList.remove('sticky-pre-nav');
  }
}

window.addEventListener('scroll', stickyNavigation);

/* 
  FUNTION PARA CAROUSEL
*/

//Capturando os elementos necessarios
var card = document.getElementById('myCard').offsetWidth;
var carousel = document.getElementById('carousel');
var numCards = document.getElementsByClassName('card').length;
var boxCarousel = document.getElementById('boxCarousel').offsetWidth;

//essas variaveis serão base para o controle
var translate = 0;
var moves = 0;

//numCardInWindow vai receber o numero de cards que podem ser exibidos na tela ao mesmo tempo
var numCardInWindow = Math.floor(boxCarousel/(card));

/* steps vai receber o numero de passos que possiveis, a logica aqui é saber quantos cards além 
 dos que já estão sendo exibidos eu tenho, neste sentido se minha tela renderizar 3 cards e eu possuir
 5 no carousel então terei 2 steps, assim em uma tela onde renderize apenas 2 cards para 5 no carousel 
 steps seria 3 */
var steps =  numCards - numCardInWindow;
var pointer = 0;
function moveCarousel(direction, referral) {
    
    if (direction == 'right') {
      // console.log('move left');
      // console.log('moves =>' + moves); 
      // console.log('steps =>' + steps); 
      moveRight();
    }else if(direction == 'left'){
      moveLeft();      
    }else{
      /*
      No mobile a navegação se dá por meio dos ponto, para isso não há necessidade de guardar os passos, basta setar a posição exata para onde
      o carousel deve fazer o translado
      */
      switch (direction) {
        case 1:
          moveRight(0);
          break;
        case 2:
          moveRight(card);
          break;
        case 3:
          moveRight(2 * card);
          break;
        case 4:
          moveRight(3 * card);
          break;      
        default:
          break;
      }
      setActive(referral);
    } 
}

//Função para percorrer os pontos de navegação limpando quem estava ativo e setando como ativo aquele que foi clicado
function setActive(params) {
  var pointers = document.getElementsByClassName('point-control');

  for (let index = 0; index < pointers.length; index++) {
    if (pointers[index].classList.contains('active')) {
      pointers[index].classList.remove('active');
    }
  }
  
  params.classList.add('active');
}

function moveLeft(params) {
  if (moves > 0) {
    translate = translate + (card+8);//O numero 8 é um desconto no valor do padding do card
    moves--;
    console.log(translate);
    carousel.style.transform = 'translateX('+ translate +'px)';
    
  }
}

function moveRight(params) {
   /* O contexto para a logica aqui feita foi levando em consideração que a div do carousel não esta centralizada e não é infinita
      onde o elmento passa de uma ponta para a outra, logo ela tem sua extenção
      continua para o lado direito como uma row infinita.
      Assim sendo moves será o numero de passos dados, então ele se move até o maximo de steps que são os cards ainda não apresentados
      chegando no limite de steps só podera voltar para o inicio */
      if(params != null){
        translate = -(params+8);
      }else if (moves < steps) {
        
        translate = translate - (card+8);
        moves++;
        console.log(translate);
      }
      carousel.style.transform = 'translateX('+ translate +'px)';          
}

//Função para fazer o menu responsivo em toggle, logo ao clicar no menu ele vai expandir para a tela cheia e ao clicar novamente vai fechar
function navBarResponsive() {
  var menuResponsive = document.getElementById('menuResponsive');
  var iconMenu = document.getElementById('iconMenu');
  var body = document.body;
  

  menuResponsive.classList.toggle('fullMenu');

  if (iconMenu.classList.contains('icon_menu')) {
    iconMenu.classList.remove('icon_menu');
    iconMenu.classList.add('icon_close');
  }else{
    iconMenu.classList.remove('icon_close');
    iconMenu.classList.add('icon_menu');
  }

  body.classList.toggle('full-body');

  if (!(nav.className == 'sticky')) {
    stickyNavigation(true); 
  }
}





